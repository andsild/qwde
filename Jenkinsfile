pipeline {
  agent any
  stages {
    stage('Clone sources') {
      steps {
        git url: 'https://github.com/andsild/qwde.git'
          checkout([
              $class: 'GitSCM',
              branches: scm.branches,
              doGenerateSubmoduleConfigurations: true,
              extensions: scm.extensions + [[$class: 'SubmoduleOption', parentCredentials: true]],
              userRemoteConfigs: scm.userRemoteConfigs
          ])
      }
    }
    stage('Env check') {
      steps {
        sh '''
          java -version
          echo $JAVA_HOME
        '''
      }
    }

    stage('Gradle build backend') {
      when {
        changeset "backend/**"
      }
      steps {
        dir("${env.WORKSPACE}/backend") {
          withGradle {
            sh './gradlew build check --info -x test'
          }
        }
      }
    }
    stage('Gradle build frontend') {
      when {
        changeset "frontend/**"
      }
      steps {
        dir("${env.WORKSPACE}/frontend") {
          sh '''
            docker image inspect qwdefrontend:deps >/dev/null 2>&1 || docker build -t qwdefrontend:deps . -f Dockerfile_build
          '''
          script {
            docker.withRegistry('https://registry.gitlab.com', 'master-gitlab-usernamePW') {
              def customImage = docker.build("andsild/qwde/frontend:latest")
              customImage.push()
            }
          }
        }
      }
    }

    stage('Publish backend') {
      when {
        anyOf {
          changeset "backend/**"
        }
      }
      steps {
        dir("${env.WORKSPACE}/backend") {
          script {
            docker.withRegistry('https://registry.gitlab.com', 'master-gitlab-usernamePW') {
              def customImage = docker.build("andsild/qwde/backend:latest")
              customImage.push()
            }
          }
        }
      }
    }
    
    stage ('Publish frontend') {
      when {
        anyOf {
          changeset "frontend/**"
        }
      }
      steps {
        dir("${env.WORKSPACE}/frontend") {
          script {
            docker.withRegistry('https://registry.gitlab.com', 'master-gitlab-usernamePW') {
              def customImage = docker.build("andsild/qwde/frontend:latest")
              customImage.push()
            }
          }
        }
      }
    }

    stage('Deploy backend') {
      when {
        anyOf {
          changeset "backend/**"
        }
      }
      steps {
        sh '''
          (docker stop qwdebackend || true)
          (docker container rm qwdebackend || true)
          docker run -d -p 8083:8083 --name qwdebackend registry.gitlab.com/andsild/qwde/backend:latest
          '''
      }
    }

    stage('Deploy frontend') {
      when {
        anyOf {
          changeset "frontend/**"
        }
      }
      steps {
        sh '''
          (docker stop qwdefrontend || true)
          (docker container rm qwdefrontend || true)
          docker run -d -p 8083:8083 --name qwdefrontend registry.gitlab.com/andsild/qwde/frontend:latest
          '''
      }
    }
  }
}

